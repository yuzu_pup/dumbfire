﻿using System.Windows.Media;
using System.Windows.Controls;

using Dumbfire.Actions;

namespace Dumbfire.UI
{
    /// <summary>
    /// Interaction logic for ActionView.xaml
    /// </summary>
    public partial class ActionView : UserControl
    {
        private Brush _originalBackgroundColor;

        private Action _action;
        public ActionView(Action action)
        {
            InitializeComponent();
            Title.Content = action.GetTitle();
            Description.Content = action.GetDescription();

            _originalBackgroundColor = Container.Background;
        }

        public void Highlight()
        {
            Container.Background = Brushes.HighlightedBlue;
        }

        public void Unhighlight()
        {
            Container.Background = _originalBackgroundColor;
        }
    }
}
