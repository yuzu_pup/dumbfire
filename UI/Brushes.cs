﻿using System.Windows;
using System.Windows.Media;

namespace Dumbfire.UI
{
    public static class Brushes
    {
        public static Brush HighlightedBlue
        {
            get
            {
                return FromResourceDictionary("HighlightedBlue");
            }
        }
        private static Brush FromResourceDictionary(string key)
        {
            return (Brush) Application.Current.FindResource(key);
        }
    }
}
