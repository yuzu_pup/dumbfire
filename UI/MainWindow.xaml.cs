﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Dumbfire.Actions;

namespace Dumbfire.UI
{
    public delegate void OnTextEntered(string searchBoxContents);
    public delegate void OnActionSelected(Action selectedAction);

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int ActionSelectionIndex;

        public MainWindow()
        {
            InitializeComponent();

            Search.TextChanged += OnSearchTextChanged;
            Search.PreviewKeyDown += OnSearchKeyDown;

            Activated += OnActivated;
            Deactivated += OnDeactivated;

            Hotkeys.Register(this, OnOpenHotKeyPressed);
        }

        protected void OnOpenHotKeyPressed()
        {
            ShowWindow();
        }

        private void ShowWindow()
        {
            WindowState = WindowState.Normal;
            Activate();
            Show();
            Search.Focus();
            Search.Text = null;
        }

        protected void OnActivated(object sender, System.EventArgs args)
        {
            Search.Focus();
        }

        protected void OnDeactivated(object sender, System.EventArgs args)
        {
            ResultsPopup.IsOpen = false;
            Hide();
        }

        private void OnSearchTextChanged(object sender, TextChangedEventArgs eventArgs)
        {
            ActionSelectionIndex = 0;
            ResultsPopup.IsOpen = Search.Text.Length > 0;
            if (Search.Text.Length > 0)
            {
                ResultsStack.Children.Clear();
                for (int ii = 0; ii < Search.Text.Length; ii++)
                {
                    ResultsStack.Children.Add(
                        new ActionView(new DebugAction
                        {
                            Title = $"Action {ii}",
                            Description = "Dude, nice"
                        })
                    );
                }
                (ResultsStack.Children[0] as ActionView).Highlight();
            }
        }



        private void OnSearchKeyDown(object sender, KeyEventArgs evt)
        {
            if(evt.Key == Key.Escape)
            {
                this.Hide();
            }
            if (ResultsStack.Children.Count > 0)
            {
                if (evt.Key == Key.Down)
                {
                    ChangeActionSelectionIndex(1);
                }
                if (evt.Key == Key.Up)
                {
                    ChangeActionSelectionIndex(-1);
                }
            }
        }
        
        private void ChangeActionSelectionIndex(int delta)
        {

            int previousSelection = ActionSelectionIndex;
            ActionSelectionIndex += delta;
            if(ActionSelectionIndex < 0)
            {
                ActionSelectionIndex += ResultsStack.Children.Count;
            }

            ActionSelectionIndex %= ResultsStack.Children.Count;

            ActionView previousActionView = ResultsStack.Children[previousSelection] as ActionView;
            ActionView currentActionView = ResultsStack.Children[ActionSelectionIndex] as ActionView;

            previousActionView.Unhighlight();
            currentActionView.Highlight();
        }
    }
}
