﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dumbfire.Actions
{
    class DebugAction : Action
    {
        public string Title;
        public string Description;
        public bool Execute()
        {
            return true;
        }

        public string GetTitle()
        {
            return Title;
        }

        public string GetDescription()
        {
            return Description;
        }
    }
}
