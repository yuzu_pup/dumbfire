﻿using System.Diagnostics;

namespace Dumbfire.Actions
{
    public class ShortcutAction : global::Dumbfire.Actions.Action
    {
        private string ApplicationName;
        private string AbsolutePathToExecutable;

        public string GetTitle()
        {
            return ApplicationName;
        }

        public string GetDescription()
        {
            return AbsolutePathToExecutable;
        }

        public bool Execute()
        {
            try
            {
                Process.Start(AbsolutePathToExecutable);
                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
            
        }
    }
}
