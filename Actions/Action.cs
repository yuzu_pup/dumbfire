﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dumbfire.Actions
{
    public interface Action
    {
        string GetTitle();
        string GetDescription();
        bool Execute();
    }
}
